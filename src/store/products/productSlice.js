import { createSlice } from "@reduxjs/toolkit";
import { fetchListProduct } from "./productActions";

const initialState = [
  {
    id: "1",
    spaceForProduct: "",
    price: 0,
    discount: 0,
    avatar: "",
    title: "",
  },
];

const productSlice = createSlice({
  name: "productSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchListProduct.fulfilled, (state, action) => {
      return action.payload;
    });
  },
});

export const productReducer = productSlice.reducer;
