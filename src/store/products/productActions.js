import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const BASE_URL = process.env.REACT_APP_API_KEY;

export const fetchListProduct = createAsyncThunk(
  "productSlice/fetchListProduct",
  async () => {
    try {
      const { data } = await axios.get(`${BASE_URL}/products`);
      return data;
    } catch (error) {
      console.log("error", error);
    }
  }
);
