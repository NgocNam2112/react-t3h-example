import { configureStore } from "@reduxjs/toolkit";
import { cartReducer } from "./cart/cartSlice";
import { productReducer } from "./products/productSlice";
import { seachReducer } from "./search/search";

const store = configureStore({
  reducer: {
    product: productReducer,
    cart: cartReducer,
    isSearch: seachReducer,
  },
});

export default store;
