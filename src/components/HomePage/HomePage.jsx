import React, { useEffect } from "react";
import BlogItem from "../BlogItem/BlogItem";
import NavLeftContent from "../NavLeftContent/NavLeftContent";
import ProductItem from "../ProductItem/ProductItem";
import "./HomePage.css";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useDispatch, useSelector } from "react-redux";
import { fetchListProduct } from "../../store/products/productActions";
import {
  addProductIntoCart,
  updateProductInCart,
} from "../../store/cart/cartActions";
import axios from "axios";

const HomePage = () => {
  const BASE_URL = process.env.REACT_APP_API_KEY;
  const dispatch = useDispatch();
  const productList = useSelector((state) => state.product);
  const isSearch = useSelector((state) => state.isSearch);
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: <img src="/icons/slick_prev_arrow.svg" alt="slick_prev_arrow" />,
    nextArrow: <img src="/icons/slick_next_arrow.svg" alt="slick_next_arrow" />,
  };

  const handleAddProductIntoCart = (item) => {
    dispatch(addProductIntoCart({ ...item, quantity: 1, productId: item.id }));
  };

  const handleUpdateQuantity = (item) => {
    dispatch(updateProductInCart(item));
  };

  const handleCheckProductInCart = async (item) => {
    const { data } = await axios.get(`${BASE_URL}/cart?productId=${item.id}`);
    if (data.length > 0) {
      handleUpdateQuantity({ ...data[0], quantity: data[0].quantity + 1 });
      return;
    }
    handleAddProductIntoCart(item);
  };

  useEffect(() => {
    dispatch(fetchListProduct());
  }, []);

  return (
    <>
      <>
        <div className="m-banner">
          <NavLeftContent />
          <div className="list-banner">
            <img src="/images/Banner.png" alt="banner" />
            <img src="/images/Banner.png" alt="banner" />
            {/* <img src="/images/Banner.png" alt="banner" /> */}
          </div>
        </div>
        <div className="m-list-product">
          <div className="wrap__list-nav-content">
            <NavLeftContent />
            <NavLeftContent />
          </div>
          <div className="list-product">
            {!isSearch.isSearch
              ? productList.map((item, index) => (
                  <ProductItem
                    item={item}
                    key={index}
                    handleCheckProductInCart={handleCheckProductInCart}
                  />
                ))
              : productList
                  .filter(
                    (item) =>
                      item.title
                        .toLowerCase()
                        .includes(isSearch.value.toLowerCase()) && item
                  )
                  .map((item, index) => (
                    <ProductItem
                      item={item}
                      key={index}
                      handleCheckProductInCart={handleCheckProductInCart}
                    />
                  ))}
          </div>
        </div>
        <div>
          <div className="m-list-blog">
            <h1>Our customers says</h1>
            <button>
              Button{" "}
              <img src="/icons/black_right_arrow.svg" alt="black_right_arrow" />
            </button>
          </div>
          <div className="slick-list-blog">
            <Slider {...settings} className="slick_carousel">
              <BlogItem />
              <BlogItem />
              <BlogItem />
              <BlogItem />
              <BlogItem />
              <BlogItem />
            </Slider>
          </div>
        </div>
        <div>
          <div className="m-list-blog">
            <h1>Section Headline</h1>
            <button>
              Button{" "}
              <img src="/icons/black_right_arrow.svg" alt="black_right_arrow" />
            </button>
          </div>
          <div className="m-section-headline">
            <ProductItem />
            <ProductItem />
            <ProductItem />
            <ProductItem />
            <ProductItem />
          </div>
        </div>
      </>
    </>
  );
};

export default HomePage;
