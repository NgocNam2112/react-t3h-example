import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchListProductInCart,
  removeProductInCart,
} from "../../store/cart/cartActions";
import CartItem from "../CartItem/CartItem";
import "./ListItemCart.css";

const ListItemCart = () => {
  const dispatch = useDispatch();
  const listCartItem = useSelector((state) => state.cart);

  const handleRemoveProductInCart = (id) => {
    dispatch(removeProductInCart(id));
  };

  useEffect(() => {
    dispatch(fetchListProductInCart());
  }, []);

  return (
    <div className="m-list-cart-container">
      <h1>Order Summary</h1>
      <p>
        Price can change depending on shipping method and taxes of your state.
      </p>

      <div className="cart-item-container">
        {listCartItem.map((item, index) => (
          <CartItem
            item={item}
            key={index}
            handleRemoveProductInCart={handleRemoveProductInCart}
          />
        ))}
      </div>

      <div className="cart-billing-container">
        <div className="cart-subtal">
          <div>
            <p>Subtotal</p>
            <p>73.98 USD</p>
          </div>
          <div>
            <p>Tax</p>
            <p>17% 16.53 USD</p>
          </div>

          <div>
            <p>Shipping</p>
            <p>0 USD</p>
          </div>
        </div>
        <div className="cart-apply-code">
          <input type="text" placeholder="Apply promo code" />
          <img src="/icons/Apply _now.svg" alt="Apply _now" />
        </div>

        <div className="cart-total-order">
          <div>
            <h3>Total Order</h3>
            <p>Guaranteed delivery day: June 12, 2020</p>
          </div>
          <h1>89.84 USD</h1>
        </div>
      </div>
    </div>
  );
};

export default ListItemCart;
