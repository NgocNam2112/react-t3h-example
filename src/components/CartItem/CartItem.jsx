import React, { useState } from "react";
import "./CartItem.css";

const CartItem = ({ item, handleRemoveProductInCart }) => {
  const [loaded, setLoaded] = useState(false);

  return (
    <div className="CartItem_container">
      <div className="cart-item">
        {!loaded && (
          <img src="/images/thumb_cart_item.png" alt="thumb_cart_item.png" />
        )}
        <img
          src={item.avatar}
          alt="product"
          onLoad={() => setLoaded(true)}
          style={{ display: loaded ? "block" : "none" }}
        />

        <div className="cart-product-item-infor">
          <h3>{item.title}</h3>
          <div>
            <span>Farm:</span>
            <p>Tharamis Farm</p>
          </div>
          <div>
            <span>Freshness:</span>
            <p>1 day old</p>
          </div>
        </div>
      </div>

      <div className="cart-item">
        <div className="cart-bonous-item">
          <div>
            <img src="/icons/wishlist.svg" alt="wishlist" />
            <span>Wishlist</span>
          </div>
          <div>
            <img src="/icons/compare.svg" alt="compare" />
            <span>Compare</span>
          </div>
          <div style={{ cursor: "pointer" }} onClick={() => {handleRemoveProductInCart(item.id)}}>
            <img src="/icons/remove.svg" alt="compare" />
            <span>Remove</span>
          </div>
        </div>
        <div className="c-product-price-container">
          <img src="/icons/rate-review.svg" alt="rate-review" />
          <div className="cart-product-price">
            <div className="c-product-price">
              <h2>
                {Math.round(item.price * (1 - 0.01 * item.discount) * 100) /
                  100}{" "}
                USD
              </h2>
              <h3>{item.price} USD</h3>
            </div>
            <div className="c-product-unit">
              <input type="text" placeholder={item.quantity} />
              <div>
                Pcs{" "}
                <img src="/icons/down_green_arrow.svg" alt="down_green_arrow" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartItem;
