import React, { useState } from "react";
import "./ProductItem.css";

const ProductItem = ({ item, handleCheckProductInCart }) => {
  const [loaded, setLoaded] = useState(false);

  return (
    <div className="product-item">
      {!loaded && (
        <img src="/images/thumb_product.png" alt="thumb_product.png" />
      )}

      <img
        src={item?.avatar}
        alt="product"
        onLoad={() => setLoaded(true)}
        style={{ display: loaded ? "block" : "none" }}
      />
      <h2>{item?.title}</h2>
      <p>{item?.spaceForProduct}</p>
      <div className="p__infor">
        <h2>{item?.price} USD</h2>
        <button onClick={() => handleCheckProductInCart(item)}>Buy now</button>
      </div>
    </div>
  );
};

export default ProductItem;
