import React, { useState } from "react";
import "./BillInfo.css";

const BillInfo = () => {
  const [isCheckbox, setIsCheckbox] = useState(false);
  const [isNoSpam, setIsNoSpam] = useState(false);
  const [agreeTerm, setAgreeTerm] = useState(false);

  const handleCheckbox = () => {
    setIsCheckbox(!isCheckbox);
  };

  const handleNoSpam = () => {
    setIsNoSpam(!isNoSpam);
  };

  const handleAgreeTerm = () => {
    setAgreeTerm(!agreeTerm);
  };
  return (
    <div className="m-billing-info">
      <h2>Billing info</h2>
      <div className="m-billing-info-step">
        <p>Please enter your billing info</p>
        <p>Step 1 of 3</p>
      </div>
      <div className="m-billing-field">
        <div className="m__small-billing-field">
          <h3>First name</h3>
          <input type="text" placeholder="First name" />
        </div>
        <div className="m__small-billing-field">
          <h3>Last name</h3>
          <input type="text" placeholder="Last name" />
        </div>
      </div>

      <div className="m-billing-field">
        <div className="m__small-billing-field">
          <h3>Email address</h3>
          <input type="text" placeholder="Email address" />
        </div>
        <div className="m__small-billing-field">
          <h3>Phone number</h3>
          <input type="text" placeholder="Phone number" />
        </div>
      </div>

      <div className="m-billing-field">
        <div className="m__small-billing-field">
          <h3>Address</h3>
          <input type="text" placeholder="Address" />
        </div>
        <div className="m__small-billing-field">
          <h3>Town / City</h3>
          <input type="text" placeholder="Town / City" />
        </div>
      </div>

      <div className="m-billing-field">
        <div className="m__small-billing-field">
          <h3>State / Country</h3>
          <input type="text" placeholder="State / Country" />
        </div>
        <div className="m__small-billing-field">
          <h3>ZIP/Postal code</h3>
          <input type="text" placeholder="ZIP/Postal code" />
        </div>
      </div>

      <div className="m__billing-checkbox">
        <img
          src={isCheckbox ? "/icons/Checkboxes.svg" : "/icons/Uncheckboxes.svg"}
          alt="Uncheckboxes"
          onClick={handleCheckbox}
        />
        <p>Ship to a different address?</p>
      </div>

      <div className="m-billing-infor-container">
        <h2>Additional informations</h2>
        <div className="m-billing-info-step">
          <p>Need something else? We will make it for you!</p>
          <p>Step 4 of 3</p>
        </div>

        <p>Order notes</p>
        <textarea
          name=""
          placeholder="Need a specific delivery day? Sending a gitf? Let’s say ..."
        ></textarea>
      </div>

      <div className="m-billing-infor-container">
        <h2>Confirmation</h2>
        <div className="m-billing-info-step">
          <p>
            We are getting to the end. Just few clicks and your order si ready!
          </p>
          <p>Step 3 of 3</p>
        </div>

        <div className="m-billing-nospam">
          <img
            src={isNoSpam ? "/icons/Checkboxes.svg" : "/icons/Uncheckboxes.svg"}
            alt=""
            onClick={handleNoSpam}
          />
          <p>
            I agree with sending an Marketing and newsletter emails. No spam,
            promissed!
          </p>
        </div>

        <div className="m-billing-agreeterm">
          <img
            src={
              agreeTerm ? "/icons/Checkboxes.svg" : "/icons/Uncheckboxes.svg"
            }
            alt=""
            onClick={handleAgreeTerm}
          />
          <p>I agree with our terms and conditions and privacy policy.</p>
        </div>
      </div>

      <button className="btn-complete">Complete order</button>

      <div className="m-billing-safe">
        <img src="/icons/ic-security-safety.svg" alt="" />
        <h4>All your data are safe</h4>
        <p>
          We are using the most advanced security to provide you the best
          experience ever.
        </p>
      </div>
    </div>
  );
};

export default BillInfo;
